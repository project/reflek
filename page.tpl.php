<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body<?php print $body_class; ?>>

  <div id="page">
  
    <?php if ($logo || $header || $breadcrumb): ?>
    <div id="pageHeader">
      
      <?php if ($header): ?>
      <div class="body">
        <?php print $header; ?>
      </div>
      <?php endif; ?>
      
      <?php if ($logo): ?>
      <div id="logo">
        <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
          <?php print theme('logo', $logo); ?>
        </a>
      </div><!-- /logo -->
      <?php endif; ?>

      <?php if ($shortcuts): ?>
      <div id="shortcuts"<?php print $shortcut_class; ?>>
        <?php if ($primary_links): ?>
        <div id="primary" class="headerBlock">
          <?php print theme('menu_links', $primary_links); ?>
        </div><!-- primary -->
        <?php endif; ?>
        
        <?php if ($secondary_links): ?>
        <div id="secondary" class="headerBlock">
          <?php print theme('menu_links', $secondary_links); ?>
        </div><!-- secondary -->
        <?php endif; ?>
      </div><!-- navLinks --> 
      <?php endif; ?>

      <?php if ($breadcrumb): ?>
      <div id="headerRegion">
        <?php if (!is_front): print $breadcrumb; endif;?>
      </div>
      <?php endif; ?>      
      
    </div><!-- pageHeader -->
    <?php endif; ?>
    
    <div id="pageBody">
      
      <?php if ($sidebar_left): ?>
      <div id="leftColumn" class="sideColumn">
        <div class="body">
          <?php print $sidebar_left; ?>
        </div>
      </div><!-- leftColumn -->
      <?php endif; ?>
      
      <?php if ($sidebar_right): ?>
      <div id="rightColumn" class="sideColumn">
        <div class="body">
          <?php print $sidebar_right; ?>
        </div>
      </div><!-- rightColumn -->
      <?php endif; ?>
      
      <div id="contentColumn">
        <div class="body">
          <?php if ($content_top): ?><div id="contentTop"><?php print $content_top; ?></div><?php endif; ?>
          <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
          <?php if ($title): ?><div id="title"><h1 class="title"><?php print $title; ?></h1></div><?php endif; ?>
          <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php print $messages; ?>
          <?php print $help; ?>
          <?php print $content; ?>
          <?php print $feed_icons; ?>
          <?php if ($content_bottom): ?><div id="contentBottom"><?php print $content_bottom; ?></div><?php endif; ?>
        </div>
      </div><!-- contentColumn -->
      
    </div><!-- pageBody -->
    
    
    <div id="pageFooter">
    
      <?php if ($footer_message): ?>
      <div class="body">
        <?php print $footer_message; ?>
      </div>
      <?php endif; ?>
      
    </div><!-- pageFooter -->
  
  </div><!-- page -->

  <?php print $closure ?>

</body>

</html>
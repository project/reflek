<?php



// ---------------------------------------------------------------- Hooks



/**
 * Implementation of hook_regions
 */
function reflek_regions() {
  return array(
    'left' => t('left sidebar'),
    'right' => t('right sidebar'),
    'content_top' => t('content top'),
    'content_bottom' => t('content bottom'),
    'header' => t('header'),
    'footer' => t('footer'),
  );
}



/**
 * Implementation of hook_variables
 * Provides body classes & doctype tags
 */
function _phptemplate_variables($hook, $vars) {

  if ($hook=='page'){
    // body classes
    $body_classes = array();

    if ($vars['is_front']){
      $body_classes[] = 'front';
    }
    if ($vars['sidebar_left']){
      $body_classes[] = 'leftColumn';
      $body_classes['either'] = 'column';
    }
    if ($vars['sidebar_right']){
      $body_classes[] = 'rightColumn';
      $body_classes['either'] = 'column';
    }
    if ($vars['sidebar_left'] && $vars['sidebar_right']){
      $body_classes[] = 'columns';
    }
    
    if (count($body_classes)>0){
      $vars['body_class'] = ' class="'.implode(' ', $body_classes).'"';
    }
    
    // Primary & secondary link classes
    $link_classes = array();
    
    if ($vars['primary_links']){
      $link_classes[] = 'primary';
    }
    if ($vars['secondary_links']){
      $link_classes[] = 'secondary';
    }
    
    if (count($link_classes)>0){
      $vars['shortcuts'] = TRUE;
      $vars['shortcut_class'] = ' class="'.implode(' ', $link_classes).'"';
    }
  }
  
  return $vars;
}



/**
 * Implementation of hook_painters
 */
function reflek_painters($args, $may_cache = FALSE){
  $painters = array();
  
  if ($may_cache) {

    /* reflecting main navigation */
    $painters['main nav'] = array(
      
      // Create the reflected image
      array(
        'operation' => 'reflect text',
        'text' => $args['default'],
        'font' => 'Tall film.ttf',
        'size' => 18,
        'color' => '#dd6600',
        'padding' => 1,
        'background' => '#ffffff',
        'transparent' => TRUE,
      ),

      // Draw the background
      array(
        'operation' => 'create image',
        'width' => 'width(canvas)',
        'height' => 'height(canvas)',
        'background' => '#ffffff',
        'as' => 'background',
      ),
      // Load the image we're going to use accross the background
      array(
        'operation' => 'load image',
        'filename' => '/'.path_to_theme().'/images/header_back.png',
        'as' => 'background image',
      ),
      array(
        'operation' => 'size image',
        'image' => '[background image]',
        'width' => 'width(background)',
        'height' => 'height(background image)',
      ),
      array(
        'operation' => 'draw image',
        'image' => '[background image]',
        'on' => '[background]',
      ),

      // Put the reflected image on the background
      array(
        'operation' => 'draw image',
        'merge' => 100,
        'image' => '[canvas]',
        'on' => '[background]',
      ),
      // Draw it on the canvas
      array(
        'operation' => 'draw image',
        'image' => '[background]',
      ),
    );

    /* reflecting logo */
    $painters['logo'] = array(
      
      array(
        'operation' => 'load image',
        'filename' => $args['default'],
        'as' => 'logo',
      ),
      
      array(
        'operation' => 'reflect image',
        'image' => '[logo]',
        'background' => '#ffffff',
        'height' => 0.5,
        'distance' => 5,
      ),

      array(
        'operation' => 'draw line',
        'y1' => 80,
        'y2' => 80,
        'color' => '#b9b9b9',
      ),
      
      array(
        'operation' => 'draw line',
        'y1' => 91,
        'y2' => 91,
        'color' => '#f1f1f1',
      ),
      
    );

  }
  
  return $painters;
}



// ---------------------------------------------------------------- Theme hooks



/**
 * Implementation of theme_page
 * Hooked to processes browser stylesheet & script inclusion
*/
function reflek_page($content, $show_blocks = TRUE) {
	
  _includes();
	
	return phptemplate_page($content, $show_blocks);
}



/**
 * Custom logo themeing hook
 *
 * @param string $url
 */
function reflek_logo($url){
      
    $doRender = module_exists('painter') && painter_theme_supports('logo');
    
    if ($doRender){
      $rendered = '<img src="'.painter_get_image('logo', $url).'" alt="'.t('Home').'" id="logo" />';
    } else {
      $rendered = '<img src="'.$url.'" alt="<?php print '.t('Home').'" id="logo" />';
    }
    return $rendered;
}



/**
 * Implementation of theme_menu_links
 * Hooked to provide li items with ids, as well as compositor images
 */
function reflek_menu_links($links = array(), $delimiter = ' | ') {
  if (!$links) {
    return '';
  }

  $count = 0;
  $output = "<ul>\n";
  $path = $_GET['q'];

  foreach ($links as $index => $link) {
    if ($link['href'] == $path || $link['href'] == '<front>' && drupal_is_front_page()){
      $link['attributes']['class'] = 'active';
      $li_class = ' class="active"';
    } else {
      $link['attributes']['class'] = NULL;
      $li_class = '';
    }
    
    // Hook into painter.module
    $doImage = module_exists('painter') && painter_theme_supports('main nav');
    
    if ($doImage){
      $rendered = '<img src="'.painter_get_image('main nav', $link['title']).'" alt="'.$link['title'].'"/>';
    } else {
      $rendered = $link['title'];
    }
    
    $output .= '<li id="index'.$count++.'"'.$li_class.'>'.l($rendered, $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $doImage)."</li>\n";
  }
  $output .= '</ul>';

  return $output;
}



// ---------------------------------------------------------------- Browser specific css and script inclusion


/**
 * Fetches the browser user agent id, if set
 *
 * @return String user agent id
 */
function _browserAgent(){
  return ( isset( $_SERVER['HTTP_USER_AGENT'] ) ) ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) : '';
}



/**
 * Return TRUE if the $id occurs within $browserAgent
 *
 * @param $id
 * @param $browserAgent
 * @return unknown
 */
function _isAgent($id){
  return (strlen($id) == 0) || is_numeric(strpos(strtolower(_browserAgent()), strtolower(trim($id))));
}



/**
 * Includes css & script
 * css & script within $includes will be included in the style imports iff 
 * the key string occurs within the HTTP_USER_AGENT server variable
 */
function _includes(){

  // include layout first, so that fixes can override
  drupal_add_css(path_to_theme().'/includes/layout.css', 'theme', 'all', FALSE);
  
  $styleDir = opendir(path_to_theme().'/includes');
  while (false !== ($file = readdir($styleDir))) {
    if ($file!='.' && $file!='..'){
      // Check each search key
      $fitsKeys = TRUE;
      if (substr($file, 0, 5) == 'just-'){
        $keys = explode('-', substr($file, 5, strlen($file) - 9));
        foreach($keys as $key){
          $fitsKeys &= _isAgent($key);
        }
      }
      
      if (substr($file, 0, 5) != 'just-' || $fitsKeys && substr($file, 0, 5) == 'just-'){
        if (is_numeric(strpos($file, '.css'))){
          drupal_add_css(path_to_theme().'/includes/'.trim($file), 'theme', 'all', FALSE);
        } elseif (is_numeric(strpos($file, '.js'))){
          drupal_add_js(path_to_theme().'/includes/'.$file, 'theme');
        }
      }
    }
  }
  drupal_get_css($css);
}